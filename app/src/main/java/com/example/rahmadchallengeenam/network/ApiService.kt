package com.example.rahmadchallengeenam.network

import com.example.rahmadchallengeenam.model.ResponDataTaksItem
import com.example.rahmadchallengeenam.model.ResponDataUserItem
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    //Task
    @GET("user/{id}/list")
    fun getTask(
        @Path("id") userId: String

    ) : Call<List<ResponDataTaksItem>>

    @GET("user/{id}/list")
    fun getFav(
        @Path("id") userId: String,
        @Query("category") favorite : String = "favorite"
    ) : Call<List<ResponDataTaksItem>>

    @GET("user/{id}/list/{listId}")
    fun getTaskId(
        @Path("id") userId: String,
        @Path("listId") idTask: String
    ) : Call<ResponDataTaksItem>

    @PUT("user/{id}/list/{listId}")
    fun putData(
        @Path("id") userId: String,
        @Path("listId") idTask : String,
        @Body request : ResponDataTaksItem
    ) : Call<ResponDataTaksItem>

    @DELETE("user/{id}/list/{listId}")
    fun delData(
        @Path("id") userId : String,
        @Path("listId") idTask : String
    ) : Call<String>

    //User
    @GET("user")
    fun getUser() : Call<List<ResponDataUserItem>>

    @GET("user/{id}")
    fun getUserId(
        @Path("id") userId: String
    ) : Call<ResponDataUserItem>

    @PUT("user/{id}")
    fun putUser(
        @Path("id") userId: String,
        @Body request: ResponDataUserItem
    ) : Call<ResponDataUserItem>

    @POST("user")
    fun postUser(@Body user : ResponDataUserItem) : Call<ResponDataUserItem>
}