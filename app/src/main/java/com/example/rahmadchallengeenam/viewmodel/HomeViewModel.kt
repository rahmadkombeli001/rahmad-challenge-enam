package com.example.rahmadchallengeenam.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.rahmadchallengeenam.model.ResponDataTaksItem
import com.example.rahmadchallengeenam.network.ApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val apiService: ApiService): ViewModel() {

    var allData : MutableLiveData<List<ResponDataTaksItem>?> = MutableLiveData()
    var updateData : MutableLiveData<ResponDataTaksItem?> = MutableLiveData()
    var deleteData : MutableLiveData<String?> = MutableLiveData()

    fun allLiveData() : MutableLiveData<List<ResponDataTaksItem>?>{
        return allData
    }

    fun updateLiveData() : MutableLiveData<ResponDataTaksItem?>{
        return updateData
    }

    fun deleteLiveData() : MutableLiveData<String?>{
        return deleteData
    }

    //Retrofit
    fun callAllData(userId: String){
        apiService.getTask(userId)
            .enqueue(object : Callback<List<ResponDataTaksItem>>{
                override fun onResponse(
                    call: Call<List<ResponDataTaksItem>>,
                    response: Response<List<ResponDataTaksItem>>,
                ) {
                    if (response.isSuccessful){
                        allData.postValue(response.body())
                    }else{
                        allData.postValue(null)
                    }
                }

                override fun onFailure(call: Call<List<ResponDataTaksItem>>, t: Throwable) {
                    allData.postValue(null)
                }
            })
    }

    fun callAllFavorite(userId: String){
        apiService.getFav(userId)
            .enqueue(object : Callback<List<ResponDataTaksItem>>{
                override fun onResponse(
                    call: Call<List<ResponDataTaksItem>>,
                    response: Response<List<ResponDataTaksItem>>,
                ) {
                    if (response.isSuccessful){
                        allData.postValue(response.body())
                    }else{
                        allData.postValue(null)
                    }
                }

                override fun onFailure(call: Call<List<ResponDataTaksItem>>, t: Throwable) {
                    allData.postValue(null)
                }

            })
    }

    fun callUpdateData(category : String, content : String, idTask : String, image : String, title : String, userId : String){
        apiService.putData(userId, idTask, ResponDataTaksItem(category, content, idTask, image, title, userId))
            .enqueue(object : Callback<ResponDataTaksItem>{
                override fun onResponse(
                    call: Call<ResponDataTaksItem>,
                    response: Response<ResponDataTaksItem>,
                ) {
                    if (response.isSuccessful){
                        updateData.postValue(response.body())
                    }else{
                        updateData.postValue(null)
                    }
                }

                override fun onFailure(call: Call<ResponDataTaksItem>, t: Throwable) {
                    updateData.postValue(null)
                }

            })
    }

    fun callDeleteData(userId: String, idTask: String){
        apiService.delData(userId, idTask)
            .enqueue(object : Callback<String>{
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    if (response.isSuccessful){
                        deleteData.postValue(response.body())
                    }else{
                        deleteData.postValue(null)
                    }
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    deleteData.postValue(null)
                }

            })
    }
}