package com.example.rahmadchallengeenam.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.rahmadchallengeenam.model.ResponDataUserItem
import com.example.rahmadchallengeenam.network.ApiService
import com.example.rahmadchallengeenam.repository.UserPrefRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(private val api : ApiService, application: Application) : AndroidViewModel(application) {
    //Data Store
    private val repository = UserPrefRepository(application)
    val dataUser = repository.readProto.asLiveData()

    fun updateProto(nama : String, userId : String) = viewModelScope.launch {
        repository.saveDataProto(nama, userId)
    }

    fun delProto() = viewModelScope.launch {
        repository.deleteData()
    }

    //Live Data
    var userData : MutableLiveData<ResponDataUserItem?> = MutableLiveData()
    var userList : MutableLiveData<List<ResponDataUserItem>?> = MutableLiveData()

    fun liveUser() : MutableLiveData<ResponDataUserItem?>{
        return userData
    }

    fun liveUserid() : MutableLiveData<ResponDataUserItem?>{
        return userData
    }

    fun liveUpdateUser() : MutableLiveData<ResponDataUserItem?>{
        return userData
    }

    fun liveUserList() : MutableLiveData<List<ResponDataUserItem>?>{
        return userList
    }


    //Retrofit
    fun getUser(username: String, password: String){
        api.getUser()
            .enqueue(object : Callback<List<ResponDataUserItem>>{
                override fun onResponse(
                    call: Call<List<ResponDataUserItem>>,
                    response: Response<List<ResponDataUserItem>>,
                ) {
                    if (response.isSuccessful){
                        userList.postValue(response.body()!!)
                    }else{
                        userList.postValue(null)
                    }
                }

                override fun onFailure(call: Call<List<ResponDataUserItem>>, t: Throwable) {
                    userList.postValue(null)
                }

            })
    }
    fun postDataUser(email : String, id : String, password : String, username : String){
        api.postUser(ResponDataUserItem(email, id, password, username))
            .enqueue(object : Callback<ResponDataUserItem>{
                override fun onResponse(
                    call: Call<ResponDataUserItem>,
                    response: Response<ResponDataUserItem>,
                ) {
                    if (response.isSuccessful){
                        userData.postValue(response.body())
                    }else{
                        error(response.message())
                    }
                }

                override fun onFailure(call: Call<ResponDataUserItem>, t: Throwable) {
                    userData.postValue(null)
                }
            })
    }

    fun putUser(email: String, id: String, username: String, password: String){
        api.putUser(id, ResponDataUserItem(email, id, password, username))
            .enqueue(object : Callback<ResponDataUserItem>{
                override fun onResponse(
                    call: Call<ResponDataUserItem>,
                    response: Response<ResponDataUserItem>,
                ) {
                    if (response.isSuccessful){
                        userData.postValue(response.body())
                    }else{
                        error(response.message())
                    }
                }

                override fun onFailure(call: Call<ResponDataUserItem>, t: Throwable) {
                    userData.postValue(null)
                }

            })
    }

    fun getUserbyId(id: String){
        api.getUserId(id)
            .enqueue(object : Callback<ResponDataUserItem>{
                override fun onResponse(
                    call: Call<ResponDataUserItem>,
                    response: Response<ResponDataUserItem>,
                ) {
                    if (response.isSuccessful){
                        userData.postValue(response.body())
                    }else{
                        error(response.message())
                    }
                }

                override fun onFailure(call: Call<ResponDataUserItem>, t: Throwable) {
                    userData.postValue(null)
                }

            })
    }
}